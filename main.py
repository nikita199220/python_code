#!/usr/bin/env python3

import click
from src.tower.main import Tower
import src.reporting.main as Reporting 
import json

##TODO: Apply Markd
##TODO: Get list of pending deletion Inventories and generate report
##TODO: Create Cli option for the inventory search suffix--> Done 
##TODO: Create a class for Tower methods--> Done
##TODO: Create Tests 

@click.group()
def cli():
    """ Define func """

@cli.command()
@click.option('--user', required=True, prompt=True,
              help='The username')
@click.option('--password', required=False, prompt=True, hide_input=True,  help='The user password')
@click.option('--tower_url', default='https://xyz', prompt=True, help='Enter the tower_url' )
@click.option('--tower_api_version', default='api/v2', prompt=True, help='Enter the api version of tower_api_version' )
@click.option('--inventory_search_suffix', default='srv', prompt=True, help='Enter the inventory search prefix' )
@click.option('--page-size', default='200', prompt=True, help='Enter the inventory list page size' )
def main(user, password, tower_url, tower_api_version, inventory_search_suffix, page_size):

    tower = Tower(user, password, tower_url, tower_api_version, inventory_search_suffix, max_iterations)
    token = tower.get_token()
    print(page_size)
    deleted_inventories = tower.cleanup_hosts(token=token, page_size=page_size)
    Reporting.create_report(user, tower_url, deleted_inventories)

if __name__== "__main__":
    main()