import requests
from datetime import timedelta, date
import json
from markd import Markdown

class Tower:
    def __init__(self, user, passwd, tower_url, tower_api_version, inventory_search_suffix, max_iterations):
        self.user = user
        self.passwd = passwd
        self.tower_url = tower_url
        self.tower_api_version = tower_api_version
        self.tower_api_url = f'{tower_url}/{tower_api_version}'
        self.inventory_search_suffix = inventory_search_suffix
        self.report_data = []


    def __get_inventories(self, token, pending_deletion=False, page_size=200, days=1):
        today = date.today()
        yesterday = today - timedelta(days)
        headers = {'Authorization': 'Bearer ' + token}
        url = f'{self.tower_api_url}/inventories?search={self.inventory_search_suffix}&created__lt={str(yesterday)}&page_size={page_size}&pending_deletion={pending_deletion}&order_by=id'
        try:
            response = requests.get(url, headers=headers, verify=False)
            response.raise_for_status()
            return response.json()
        except requests.exceptions.RequestException as request_error:
            print(str(request_error))
            exit(1)


    def __delete_inventory(self, inventory, token):
        headers = { 'Authorization': f'Bearer {token}' }
        try:
            response = requests.delete(f'{self.tower_api_url}/inventories/{inventory["id"]}', headers=headers, verify=False)           
            # response = requests.get(f'{self.tower_api_url}/inventories/{inventory["id"]}', headers=headers, verify=False)
            response.raise_for_status()
            self.report_data.append([str(inventory["id"]),
                                    inventory["name"], inventory["created"],
                                    inventory["modified"], str(inventory["organization"]),
                                    str(response.status_code)])
        except requests.exceptions.RequestException as request_error:
            ### ToDo: Identify pending deletion inventories,  save them in memory so we dont retry to delete them in next loop
            print(str(request_error))
            self.report_data.append([inventory["id"],
                                    inventory["name"], inventory["created"],
                                    inventory["modified"], inventory["organization"],
                                    str(request_error)])


    def get_token(self):
        try:
            creds = (self.user, self.passwd)
            response = requests.post(f'{self.tower_api_url}/tokens/', auth=creds, verify=False)
            response.raise_for_status()
            token_json = response.json()
            token = token_json["token"]
            return token
        except requests.exceptions.RequestException as request_error:
            print(str(request_error))
            exit(1)

    

    def cleanup_hosts(self, token, page_size=200):
        inventories_list = self.__get_inventories(token=token, page_size=page_size)
        if (inventories_list["count"] == 0):
                print("Inventory count is zero so no more inventories left for deletion")
                return self.report_data
            for inventory in inventories_list["results"]: 
                self.__delete_inventory(inventory, token)
                print(f'Inventory {inventory["id"]} deleted')
            return self.cleanup_hosts(token, page_size)
        
        return self.report_data