from markd import Markdown
from datetime import datetime
import time
import itertools
import os

def create_report(user, tower_url, data):
    markd = Markdown()
    markd.add_header(
        f'Tower Hosts Clean up Report {datetime.now()}', 1)
    markd.add_text(markd.emphasis(f'Deleted Inventories on {tower_url}'))
    markd.add_text(f'Executed by {user}')  
    table_header = [["Inventory ID", "Inventory Name", "Created", "Last Used", "Organization", "Status"]]
    table_rows = table_header + data
    markd.add_table(*table_rows)
    markd.save(f'{os.getcwd()}/inventory_delete_report_{time.strftime("%Y_%m_%d_%H%M%S")}.md')