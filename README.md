Name
Delete inventories from ansible tower using REST API's.
Description:
Code will perform the following:
1. Create the token for the authorized user who has admin previledge 
2. Get the list of all the inventories which are required to be deleted from the ansible tower.
    --> inventory list will generated on the basis of specific criteria by applying the filters.
    Example: inventories?search={self.inventory_search_suffix}&created__lt={str(yesterday)}&page_size={page_size}&pending_deletion={pending_deletion}&order_by=id'
2. Perform the deletion of all the filtered invetories list.
3. Create the report of all the  deleted inventories. 

Requirements/Prerequisites
1. Packages click, Markdown, datetime, time
2. Input parameter --> (user, password, tower_url, tower_api_version, inventory_search_suffix)
username, password, tower_url, tower_api_version, inventory_search_suffix

Installation/how to install the things

Configuration/
Usage/how to use
General notes
bugs
